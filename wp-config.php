<?php

// If a local config file exists
if ( file_exists( dirname( __FILE__ ) . '/wp-config-local.php' ) ) {
	// Use these settings on the local server
 	include( dirname( __FILE__ ) . '/wp-config-local.php' );
} else {
	// Otherwise use the settings below on staging/production
	define('WP_HOME', 'http://firewire.fluidstaging.com');
	define('WP_SITEURL', WP_HOME);

	// ** MySQL settings ** //
	/** The name of the database for WordPress */
	define('DB_NAME', 'db_firewire');

	/** MySQL database username */
	define('DB_USER', 'forge');

	/** MySQL database password */
	define('DB_PASSWORD', 'DGsAZEDytb5cpKX6aTjN');

	/** MySQL hostname */
	define('DB_HOST', 'localhost');

	/** Define the environment, for Roots/Sage */
	define('WP_ENV', 'production');
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don\'t change this if in doubt. */
define('DB_COLLATE', '');

define('AUTH_KEY',         'LI0pJoKSb4JVLAF+fDqAlAEjb1kUw4EoZRK0fQRDBdp1SMeBX8ym/5A81JIP3+Y0JjAPDqxBEL9Gl6pleeJeOQ==');
define('SECURE_AUTH_KEY',  'XVAthO02bqVMeBy+1usPe7mpDo7RUfswkuTk9vuYdwPaFwP37BhActDeRxROspcFfE8SqJI5UjDRLJk/5F00nQ==');
define('LOGGED_IN_KEY',    'HDWj+Pqqap2DAnfl2PkucYhBvdNrs5hW2ua609uRiBFOGJe4ku7NTFmi5X1Yub9DzjthdD/EGMM3iw5P+KxdWw==');
define('NONCE_KEY',        'ljNyX0oKqsHWv2mM0E3s59p21jN5RpNvIfQuDvJMDKmLspOZXOZ/SMY3zkZ0j6DzCzs68VG9LosC4ksazd59aA==');
define('AUTH_SALT',        'op0bOvnum52hXfekvy7X9ozd3KfZPPIWg4LKQQcQIAWakDc3ZXVY3GndWMwk3qgHGlWGDAVA9uGTUHwIaCRnVA==');
define('SECURE_AUTH_SALT', 'KNjoDQkVgyie5DpibFAl8sad+QadmV2RS5P7rsBDy/T2o/jbPPfD3VGsIouCDKevOX2sIzGDp42oloWdowPDAg==');
define('LOGGED_IN_SALT',   'xRZtavosr6U8dQNyv6uOnwU2xLY87xdt9ZlsYgWKn+TowNNWc5k5SpbaAPRj1cjyV5ThRYvSh4nFX7AiUbjNbA==');
define('NONCE_SALT',       'zLA/+08h/4GIWWoAUUErkb5kWkVm/rgSG25CfKdPUrrYSKmkjhrpfg650rVZevTXFpfw6I18PVRQYEYliY60JQ==');

$table_prefix = 'fd_';

define( 'WP_DEBUG', false );
define( 'WP_DEBUG_LOG', false );
define( 'AUTOMATIC_UPDATER_DISABLED', false );
define( 'WP_AUTO_UPDATE_CORE', true );
define( 'WPCF7_AUTOP', false );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
