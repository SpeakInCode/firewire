import select2 from 'select2'

export default {
  init() {

    // Form Toggle Button
    $('.form-toggle a').click(function (event) {
      event.preventDefault()
      $('section.form').slideToggle()
      $(this).toggleClass('rotate')
    })

    // Custom checkboxes
    $('input[type=checkbox]').click(function () {
      if ($(this).prop('checked')) {
        $(this).next().addClass('checked')
      }
      if (!$(this).prop('checked')) {
        $(this).next().removeClass('checked')
      }
    })

    // Select2 jQuery plugin for better styling
    $('select').select2({
      minimumResultsForSearch: -1
    })

    // Custom date picker placeholder
    $('input[type=date]').focus(function () {
      $(this).addClass('focused')
    })
    $('input[type=date]').blur(function () {
      if (!$(this).val()) {
        $(this).removeClass('focused')
      }
    })

    // Textarea Resize
    $('textarea').focus(function () {
      $(this).addClass('focused')
    })

    $('textarea').blur(function () {
      if (!$(this).val()) {
        $(this).removeClass('focused')
      }
    })

    // Acceptance check by default
    $('.stay_updated .wpcf7-list-item-label').addClass('checked')

  }
}
