export default {
  init() {

    // Product Slider
    $('.image-slider').slick({
      arrows: false,
      dots: false,
      autoplay: true,
      autoplaySpeed: 4000,
      fade: true,
      pauseOnHover: false
    })

    // Testimonial Slider
    $('.slider').slick({
      arrows: false,
      dots: true,
      autoplay: true,
      autoplaySpeed: 5000,
      fade: true
    })

    // When banner form is submitted
    document.addEventListener( 'wpcf7mailsent', function( event ) {
      $('.file').show()
      $('.form').hide()
    }, false );

  }
};
