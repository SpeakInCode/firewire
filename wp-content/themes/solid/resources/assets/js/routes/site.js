export default {
  init() {

    // Custom Hamburger Icon
    $('.hamburger-menu').on('click', function() {
      $('.bar', this).toggleClass('animate')
      $('nav.top-bar').slideToggle()
    });

  }
};
