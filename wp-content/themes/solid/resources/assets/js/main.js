/** Import external dependencies */
import $ from 'jquery'
import 'slick-carousel'

/** Import Foundation */
import './_foundation'

/** Import local dependencies */
import Router from './util/Router'
import site from './routes/site'
import home from './routes/home'
import pageContact from './routes/contact'

/** Populate Router instance with DOM routes */
const routes = new Router({
  site,
  home,
  pageContact
});

/** Load events */
jQuery(document).ready(() => routes.loadEvents())
