{{--
  Template Name: Work
--}}

@extends('layouts.app')

@section('content')

  <section class="our-work">
    <div class="text-center">
      <h1 class="page-heading">{{ the_field('page_heading') }}</h1>
    </div>
    <div class="row align-center">
      <div class="large-5 tablet-7 mini-9 small-12 columns">
        <div class="content-ready">
          @if(get_field('blue_intro'))
            <h5>{{ the_field('blue_intro') }}</h5>
          @endif
          @if(get_field('intro'))
            <div class="copy copy--right">
              {{ the_field('intro') }}
            </div>
          @endif
        </div>
      </div>
    </div>
    <div class="row align-center">
      <div class="tablet-11 small-12 columns">
        <div class="products">
          @repeater('work')
            <div class="meta">
              <p class="blue">{{ the_sub_field('book_name') }}</p>
              <p>{{ the_sub_field('author') }}</p>
            </div>
            <div class="images">
              @repeater('images')
              @set($image = get_sub_field('image'))
                <img src="{{ $image['url'] }}" alt="{{ $image['alt'] }}">
              @endrepeater
            </div>
          @endrepeater
        </div>
      </div>
    </div>
  </section>

@endsection
