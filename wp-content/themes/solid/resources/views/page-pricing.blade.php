{{--
  Template Name: Pricing
--}}

@extends('layouts.app')

@section('content')

  <section class="pricing-guidelines">
    <div class="text-center">
      <h1 class="page-heading">{{ the_field('page_heading') }}</h1>
    </div>
    <div class="row align-center">
      <div class="large-5 tablet-7 mini-9 small-12 columns">
        <div class="content-ready">
          @if(get_field('blue_intro'))
            <h5>{{ the_field('blue_intro') }}</h5>
          @endif
          @if(get_field('intro'))
            <div class="copy copy--right">
              {{ the_field('intro') }}
            </div>
          @endif
        </div>
        <div class="content-main">
          {{ the_field('main_content') }}
        </div>
      </div>
    </div>
    <div class="cta">
      <a href="{{ the_field('button_link') }}" class="button">
        {{ the_field('button_text') }}
      </a>
    </div>
  </section>

  <section class="banner-image" style="background-image:url({{ the_field('banner_image') }});"></section>

@endsection
