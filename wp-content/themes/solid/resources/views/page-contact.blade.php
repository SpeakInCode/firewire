{{--
  Template Name: Contact
--}}

@extends('layouts.app')

@section('content')

  <section class="contact-us">
    <div class="text-center">
      <h1 class="page-heading">{{ the_field('page_heading') }}</h1>
    </div>
    <div class="row align-center">
      <div class="large-5 tablet-7 mini-9 small-12 columns">
        <div class="content-ready">
          @if(get_field('blue_intro'))
            <h5>{{ the_field('blue_intro') }}</h5>
          @endif
          @if(get_field('intro'))
            <div class="copy copy--right">
              {{ the_field('intro') }}
            </div>
          @endif
        </div>
        <div class="content-main address">
          <h3>{{ the_field('address_title') }}</h3>
          <div class="street">{{ the_field('address') }}</div>
          @php($string = get_field('phone_number'))
          @php($number = preg_replace("/[^0-9]/", '', $string))
          <a class="phone" href="tel:{{ $number }}">{{ $string }}</a><br>
          <a class="email" href="mailto:{{ the_field('email') }}">{{ the_field('email') }}</a>
        </div>
        <div class="form-toggle">
          <a class="button">{{ the_field('form_toggle_button_text') }}</a>
        </div>
      </div>
    </div>
  </section>

  <section class="form">
    <div class="row align-center">
      <div class="large-6 tablet-7 medium-9 mini-11 small-12 columns">
        @shortcode('[contact-form-7 id="188" title="Contact"]')
      </div>
    </div>
  </section>

  <section class="map">
    <div class="row column">
      @shortcode('[wpgmza id="1"]')
    </div>
  </section>

@endsection


