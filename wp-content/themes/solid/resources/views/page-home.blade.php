{{--
  Template Name: Home
--}}

@extends('layouts.app')

@section('content')

  <section class="hero" style="background-image:url({{ the_field('hero_background') }});">
    <div class="row align-center">
      <div class="small-12 columns">
        <div class="headings">
          <h1>{{ the_field('hero_heading') }}</h1>
          @if(get_field('hero_subheading'))
            <h3>{{ the_field('hero_subheading') }}</h3>
          @endif
        </div>
      </div>
    </div>
    <div class="row align-center">
      <div class="tablet-10 medium-11 small-12 columns">
        <div class="cta">
          <a href="{{ the_field('hero_button_link') }}" class="button">
            {{ the_field('hero_button_text') }}
          </a>
        </div>
      </div>
    </div>
  </section>

  @flexcontent('home_layouts')
    @layout('copy_top_image_bottom')
      <section class="featured-product">
        <div class="row align-center">
          <div class="tablet-6 medium-8 mini-10 small-12 columns">
            @if(get_sub_field('blue_intro'))
              <h5>{{ the_sub_field('blue_intro') }}</h5>
            @endif
            <div class="content">
              {{ the_sub_field('copy') }}
            </div>
          </div>
        </div>
        <div class="row column">
          <div class="image-slider">
            @repeater('image_slider')
              <div class="image">
                @set($image = get_sub_field('image'))
                <img src="{{ $image['url'] }}" alt="{{ $image['alt'] }}">
              </div>
            @endrepeater
          </div>
        </div>
      </section>
    @endlayout

    @layout('banner_form')
      <section class="banner-form" style="background-image:url({{ the_sub_field('banner_background') }});">
        <div class="row align-center">
          <div class="medium-5 small-12 columns">
            <h3>{{ the_sub_field('copy_left') }}</h3>
          </div>
          <div class="medium-5 small-12 columns text-right">
            <div class="form">
              @shortcode('[contact-form-7 id="70" title="Banner Download Form"]')
            </div>
            <div class="file">
              <a class="button button--white" href="{{ the_sub_field('file_to_be_downloaded') }}" download>{{ the_sub_field('file_download_button_text') }}</a>
            </div>
          </div>
          <div class="small-12 columns">
            <h5>{{ the_sub_field('copy_bottom') }}</h5>
          </div>
        </div>
      </section>
    @endlayout

    @layout('centered_list')
      <section class="centered-list">
        <div class="row column">
          <div class="content">
            @if(get_sub_field('heading'))
              <h2>{{ the_sub_field('heading') }}</h2>
            @endif
            <ul style="background-image:url({{ the_sub_field('background_image') }});">
              @repeater('list')
                <li>{{ the_sub_field('list_item') }}</li>
              @endrepeater
            </ul>
          </div>
        </div>
      </section>
    @endlayout

    @layout('image_tiles')
      <section class="image-tiles">
        <h2>{{ the_sub_field('heading') }}</h2>
        <div class="row align-center">
          <div class="large-9 tablet-11 small-12 columns">
            <div class="image-tiles">
              <div class="row small-up-2 medium-up-3">
                @set($images = get_sub_field('image_tiles'))
                @if ($images)
                  @foreach ($images as $image)
                    <div class="column">
                      <div class="tile">
                        <div>
                          <img src="{{ $image['url'] }}" alt="{{ $image['alt'] }}">
                        </div>
                      </div>
                    </div>
                  @endforeach
                @endif
              </div>
            </div>
          </div>
        </div>
      </section>
    @endlayout

    @layout('testimonial_slider')
      <section class="testimonials">
        <div class="row align-center">
          <div class="large-5 tablet-7 medium-9 mini-11 small-12 columns">
            <div class="slider" data-equalizer>
              @repeater('testimonials')
                <div class="content" data-equalizer-watch>
                  <div>
                    @if(get_sub_field('large_blue_intro'))
                      <h5>{{ the_sub_field('large_blue_intro') }}</h5>
                    @endif
                    <p>
                      {{ the_sub_field('testimonial') }}
                      <strong>{{ the_sub_field('testimonial_author') }}</strong>
                    </p>
                  </div>
                </div>
              @endrepeater
            </div>
            <div class="cta">
              <a href="{{ the_sub_field('button_link') }}" class="button">
                {{ the_sub_field('button_text') }}
              </a>
            </div>
          </div>
        </div>
      </section>
    @endlayout
  @endflexcontent

@endsection
