<!doctype html>
<html class="no-js" @php(language_attributes())>
  @include('partials.head')
  <body @php(body_class())>
    @stack('body')
    {{ the_field('body_script_snippets', 'option') }}

    @include('partials.nav')

      <main class="site {{ main_class() }}" role="document">
        @yield('content')
      </main>

    @include('partials.footer')

    @php(wp_footer())
    {{ the_field('footer_script_snippets', 'option') }}
    @stack('footer')
  </body>
</html>
