{{--
  Template Name: About
--}}

@extends('layouts.app')

@section('content')

  <section class="about-content">
    <div class="text-center">
      <h1 class="page-heading">{{ the_field('page_heading') }}</h1>
    </div>
    <div class="row">
      <div class="medium-6 small-12 columns medium-order-1 small-order-2">
        <div class="content-ready">
          @if(get_field('image'))
            @set($image = get_field('image'))
            <img src="{{ $image['url'] }}" alt="{{ $image['alt'] }}">
          @endif
          @if(get_field('blue_caption'))
            <p class="blue">{{ the_field('blue_caption') }}</p>
          @endif
          @if(get_field('secondary_caption'))
            <p>{{ the_field('secondary_caption') }}</p>
          @endif
          @if(get_field('content_left'))
            <div class="copy copy--left">
              {{ the_field('content_left') }}
            </div>
          @endif
        </div>
      </div>
      <div class="medium-6 small-12 columns medium-order-2 small-order-1">
        <div class="content-ready">
          @if(get_field('blue_intro'))
            <h5>{{ the_field('blue_intro') }}</h5>
          @endif
          @if(get_field('content_right'))
            <div class="copy copy--right">
              {{ the_field('content_right') }}
            </div>
          @endif
          @if(get_field('list_heading'))
            <h4>{{ the_field('list_heading') }}</h4>
          @endif
          @if(get_field('list'))
            <ul>
              @repeater('list')
                <li>{{ the_sub_field('list_item') }}</li>
              @endrepeater
            </ul>
          @endif
        </div>
        <div class="cta">
          <a href="{{ the_field('button_link') }}" class="button">
            {{ the_field('button_text') }}
          </a>
        </div>
      </div>
    </div>
  </section>

  <section class="banner-image" style="background-image:url({{ the_field('banner_image') }});"></section>

@endsection
