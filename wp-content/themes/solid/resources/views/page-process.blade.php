{{--
  Template Name: Process
--}}

@extends('layouts.app')

@section('content')

  <section class="our-process">
    <div class="text-center">
      <h1 class="page-heading">{{ the_field('page_heading') }}</h1>
    </div>
    <div class="row align-center">
      <div class="tablet-5 medium-6 small-12 columns">
        <div class="images">
          @repeater('images')
          @set($image = get_sub_field('image'))
            <img src="{{ $image['url'] }}" alt="{{ $image['alt'] }}">
          @endrepeater
        </div>
      </div>
      <div class="tablet-5 medium-6 small-12 columns">
        <div class="content-ready">
          @if(get_field('blue_intro'))
            <h5>{{ the_field('blue_intro') }}</h5>
          @endif
          @if(get_field('content'))
            <div class="copy copy--right">
              {{ the_field('content') }}
            </div>
          @endif
          @if(get_field('list_heading'))
            <h4>{{ the_field('list_heading') }}</h4>
          @endif
          @if(get_field('list'))
            <ul class="processes">
              @repeater('list')
              <li><i class="fa fa-angle-right"></i>{{ the_sub_field('list_item') }}</li>
              @endrepeater
            </ul>
          @endif
        </div>
        <div class="cta">
          <a href="{{ the_field('button_link') }}" class="button">
            {{ the_field('button_text') }}
          </a>
        </div>
      </div>
    </div>
  </section>

  <section class="banner-image" style="background-image:url({{ the_field('banner_image') }});"></section>

@endsection
