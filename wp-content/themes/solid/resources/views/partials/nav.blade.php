@php
  $breakpoint = 'large';
  $topbar_logo = get_field('header_logo', 'option');
@endphp

<header id="site-header">

  {{-- Mobile Navbar --}}
  <nav class="title-bar" data-responsive-toggle="top-bar-menu" data-hide-for="{{ $breakpoint }}">
    <div class="title-bar-left">
      <a href="{{ home_url('/') }}" class="nav-logo">
        <img src="{{ $topbar_logo }}" alt="{{ get_bloginfo('name', 'display') }}">
      </a>
    </div>

    @if (has_nav_menu('primary_navigation'))
      <div class="title-bar-right">
        <ul class="menu">
          <li class="hamburger-menu">
            <div class="bar"></div>
          </li>
        </ul>
      </div>
    @endif
  </nav>

  {{-- Desktop Navbar --}}
  <nav class="top-bar" id="top-bar-menu">
    <div class="top-bar-row">
      <div class="top-bar-left show-for-{{ $breakpoint }}">
        <ul class="menu menu-logo">
          <li><a href="{{ home_url('/') }}"><img src="{{ $topbar_logo }}" alt="{{ get_bloginfo('name', 'display') }}"></a></li>
        </ul>
      </div>
      @if(has_nav_menu('primary_navigation'))
        <div class="top-bar-right">
            <ul class="vertical {{ $breakpoint }}-horizontal menu menu-items" data-responsive-menu="accordion {{ $breakpoint }}-dropdown" data-parent-link="true" data-submenu-toggle="true">
              {{-- Numbers before menu items added in /wp-content/themes/solid/app/theme/navigation.php line 51 --}}
              {!! wp_nav_menu(['items_wrap' => '%3$s', 'theme_location' => 'primary_navigation', 'walker' => new ZurbNavigation()])!!}
            </ul>
        </div>
      @endif
    </div>
  </nav>

  {{-- Drop Shadow --}}
  <div class="shadow"></div>
</header>

