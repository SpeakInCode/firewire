<footer class="site-footer" role="contentinfo">
  <div class="row">
    <div class="tablet-6 small-12 columns">
      <div class="footer-block left">
        <div class="logo">
          <img src="{{ the_field('footer_logo', 'option') }}">
        </div>
        <p>Copyright © {{ date('Y') }} {{ the_field('footer_copyright', 'option') }}</p>
      </div>
    </div>
    <div class="tablet-6 small-12 columns">
      <div class="footer-block right">
        <a href="mailto:{{ the_field('footer_email', 'option') }}">{{ the_field('footer_email', 'option') }}</a>
        @php($string = get_field('footer_phone_number', 'option'))
        @php($number = preg_replace("/[^0-9]/", '', $string))
        <a href="tel:{{ $number }}">{{ $string }}</a>
      </div>
    </div>
  </div>
</footer>
