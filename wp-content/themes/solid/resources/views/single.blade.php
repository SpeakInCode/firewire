@extends('layouts.app')

@section('content')
  @loop
    @include('partials.content-single-'.get_post_type())
  @endloop
@endsection
