<?php

/**
 * Custom Blade Directives
 */
add_action('after_setup_theme', function () {
    /**
     * Create @repeater() Blade directive
     * @param string $field The ACF Repeater Field name
     */
    App\sage('blade')->compiler()->directive('repeater', function ($field) {
        return "<?php if(have_rows($field)): while(have_rows($field)): the_row(); ?>";
    });

    /**
     * Create @endrepeater Blade directive
     */
    App\sage('blade')->compiler()->directive('endrepeater', function () {
        return "<?php endwhile; endif; ?>";
    });

    /**
     * Create @flexcontent() Blade directive
     * @param string $field The ACF Flexible Content Field name
     */
    App\sage('blade')->compiler()->directive('flexcontent', function ($field) {
        return "<?php if(have_rows($field)): while(have_rows($field)): the_row(); ?>";
    });

    /**
     * Create @endflexcontent Blade directive
     */
    App\sage('blade')->compiler()->directive('endflexcontent', function () {
        return "<?php endwhile; endif; ?>";
    });

    /**
     * Create @layout() Blade directive
     * @param string $field The ACF Layout Field name
     */
    App\sage('blade')->compiler()->directive('layout', function ($field) {
        return "<?php if(get_row_layout() == {$field}): ?>";
    });

    /**
     * Create @endlayout Blade directive
     */
    App\sage('blade')->compiler()->directive('endlayout', function () {
        return "<?php endif ?>";
    });

    /**
     * Create @relationship() Blade directive
     * @param string $field The ACF Relationship Field name
     */
    App\sage('blade')->compiler()->directive('relationship', function ($field) {
        return "<?php global \$post; \$posts = get_field({$field}); if(\$posts): foreach(\$posts as \$post): setup_postdata(\$post); ?>";
    });

    /**
     * Create @endrelationship Blade directive
     */
    App\sage('blade')->compiler()->directive('endrelationship', function () {
        return "<?php endforeach; wp_reset_postdata(); endif ?>";
    });

    /**
     * Create @loop() Blade directive
     * @param object $wp_query A new \WP_Query instance (optional)
     */
    App\sage('blade')->compiler()->directive('loop', function ($wp_query = null) {
        if ($wp_query) {
            return "<?php if({$wp_query}->have_posts()): while({$wp_query}->have_posts()): {$wp_query}->the_post(); ?>";
        } else {
            return "<?php if(have_posts()): while(have_posts()): the_post(); ?>";
        }
    });

    /**
     * Create @endloop Blade directive
     */
    App\sage('blade')->compiler()->directive('endloop', function () {
        return "<?php endwhile; endif; wp_reset_postdata(); ?>";
    });

    /**
     * Create @query() Blade directive
     * @param array $args A Wordpress query argument array
     */
    App\sage('blade')->compiler()->directive('query', function ($args) {
        return "<?php \$wp_query = null; \$wp_query = new WP_Query({$args}); if(\$wp_query->have_posts()): while(\$wp_query->have_posts()): \$wp_query->the_post(); ?>";
    });

    /**
     * Create @endquery Blade directive
     */
    App\sage('blade')->compiler()->directive('endquery', function () {
        return "<?php endwhile; endif; wp_reset_postdata(); ?>";
    });

    /**
     * Create @set Blade directive
     */
    App\sage('blade')->compiler()->directive('set', function ($expression) {
        return "<?php {$expression}; ?>";
    });


    /**
     * Create @shortcode Blade directive
     */
    App\sage('blade')->compiler()->directive('shortcode', function ($expression) {
        return "<?php echo do_shortcode({$expression}); ?>";
    });
});
