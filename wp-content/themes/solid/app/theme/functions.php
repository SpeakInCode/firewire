<?php

/**
 * Laravel Mix Manifest tracker
 * @param  string $asset Path to the Mix'd asset
 * @return string        Path to the assed file
 */
function mix($asset)
{
    $manifest = App\config('theme.dir') . '/public/mix-manifest.json';

    if (file_exists($manifest)) {
        $asset_paths = json_decode(file_get_contents($manifest));
        $asset = $asset_paths->{$asset};
    }

    return ltrim($asset, '/');
}

/**
 * Add a class to the <main> element of the current page slug
 *
 * @return string CSS Class
 */
 function main_class() {
    $query = get_queried_object();
    $page_class = 'default';


    if (is_archive()) {
        if (is_category()) {
            $page_class = $query->taxonomy . ' ' . $query->slug;
        } elseif (is_tax()) {
            $page_class = str_replace('_', '-', $query->taxonomy);
        } else {
            $page_class =  str_replace('/', '-', $query->rewrite['slug']);
        }
    } elseif (is_single()) {
        $post_type_slug = str_replace('_', '-', $query->post_type);
        $page_class = $post_type_slug . '-single';
    } elseif (is_page()) {
        $template_path = str_replace('.blade.php', '', get_page_template_slug($post->ID));
        $page_class = str_replace('views/page-', '', $template_path);
        if (!$page_class) {
            $page_class = 'default';
        }
    } elseif (is_404()) {
        $page_class = 'four-oh-four';
    } elseif (is_home()) {
        $page_class = 'blog';
    }

    return $page_class;
}

/**
 * Generate a URL to the theme images folder
 *
 * @param string $img Image name and extension e.g. icon.png
 * @return string Image URL
 */
function the_img($img)
{
    return App\asset_path("img/{$img}");
}

/**
 * Add ACF Options page
 */
if (function_exists('acf_add_options_page')) {
    acf_add_options_page([
        'menu_title'  => 'Global Content',
        'menu_slug'   => 'global-content',
        'capability'  => 'edit_posts',
        'redirect'    => true,
        'icon_url'    => 'dashicons-admin-site',
    ]);

    acf_add_options_sub_page([
        'page_title'  => 'Site General Settings',
        'menu_title'  => 'General',
        'parent_slug' => 'global-content',
    ]);

    acf_add_options_sub_page([
        'page_title'  => 'Site Header Settings',
        'menu_title'  => 'Header',
        'parent_slug' => 'global-content',
    ]);

    acf_add_options_sub_page([
        'page_title'  => 'Site Footer Settings',
        'menu_title'  => 'Footer',
        'parent_slug' => 'global-content',
    ]);

    acf_add_options_sub_page([
        'page_title'  => 'Site Analytics Settings',
        'menu_title'  => 'Analytics',
        'parent_slug' => 'global-content',
    ]);
}

/**
 * ACF JSON Save Point
 */
add_filter('acf/settings/save_json', function ($path) {
    $path = get_stylesheet_directory() . '/assets/fields';
    return $path;
});

/**
 * ACF JSON Load Point
 */
add_filter('acf/settings/load_json', function ($paths) {
    unset($paths[0]);
    $paths[] = get_stylesheet_directory() . '/assets/fields';
    return $paths;
});

/**
 * Allow SVG's to be uploaded in the WYSIWYG.
 */
add_filter('upload_mimes', function ($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
});

/**
 * Add Responsive Embed container around embeds
 */
add_filter('embed_oembed_html', function ($html, $url, $attr, $post_id) {
    return '<div class="responsive-embed widescreen">' . $html . '</div>';
}, 99, 4);

/**
 * Prevent FontAwesome plugin from double loading
 * the library on the front-end of the website
 */
if (!is_admin()) {
    add_filter('ACFFA_get_fa_url', function($url)  {
        $url = '';
        return $url;
    });
}

/**
 * Add Google Maps API key to ACF settings
 */
// add_action('acf/init', function() {
//     acf_update_setting('google_api_key', 'google-maps-api-key-goes-here');
// });

/**
 * Custom posts per page for a custom post type
 */
/*
add_action('pre_get_posts', function($query) {
    // Update the custom_post_type argument in the is_post_type_archive() method
    if (!is_admin() &&
        $query->is_main_query() &&
        is_post_type_archive('custom_post_type')) {
        // Update this value to match the showposts variable from $args in your query
        $query->set('posts_per_page', 5);
    }
});
*/
