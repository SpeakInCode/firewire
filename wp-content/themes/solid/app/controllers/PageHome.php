<?php

namespace App;

use Sober\Controller\Controller;

class PageHome extends Controller
{
    public function home()
    {
        return 'This is the home page!';
    }
}
