let mix = require('laravel-mix');
let webpack = require('webpack');

// Set project paths
let localDomain = 'firewire.test';
let themePath  = 'wp-content/themes/solid';
let assetsPath  = `${themePath}/resources/assets`;
let publicPath    = `${themePath}/public`;

mix // Fire up Mix

  // Custom Webpack config overrides
  .webpackConfig({
    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery'
      })
    ]
  })

  // Suppress success messages
  .disableSuccessNotifications()

  // Set configuration paths
  .setPublicPath(publicPath)
  .setResourceRoot(`${themePath}/resources`)

  // Compile Javascript (ES6)
  .js(`${assetsPath}/js/main.js`, `${publicPath}/js`)

  // Compile Sass
  .fastSass(`${assetsPath}/scss/main.scss`, `${publicPath}/css`, {
    includedPaths: ['node_modules']
  })
  .sourceMaps()

  // Setup BrowserSync
  .browserSync({
    proxy: localDomain,
    host: localDomain,
    notify: false,
    //browser: 'google chrome',
    open: false, //'external',
    reloadOnRestart: true,
    injectChanges: true,
    files: [
      `${themePath}/**/*.php`,
      `${publicPath}/**/*.js`,
      `${publicPath}/**/*.css`
    ]
  })

// Setup versioning (cache-busting)
if (mix.inProduction()) {
  mix.version();
  mix.babel(`${publicPath}/js/main.js`, `${publicPath}/js/main.js`);
}
